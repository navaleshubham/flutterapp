import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/constants/appbartitles.dart';
import 'package:my_society_digital_society/constants/getCurrentScreen.dart';
import 'package:my_society_digital_society/constants/screenNames.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
import 'package:my_society_digital_society/presentation/widgets/bottomnavigator.dart';
import 'package:my_society_digital_society/presentation/widgets/drawer.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(context.watch<MainCubit>().state.appBarTitle),
        centerTitle: true,
        actions: [
          IconButton(
            icon: Icon(Icons.notifications_none_outlined),
            onPressed: () => {
              context
                  .read<MainCubit>()
                  .screenChange(notification, notificationstitle),
            },
          ),
        ],
      ),
      drawer: DashboardDrawer(),
      bottomNavigationBar: DashboardBottomNav(),
      drawerEdgeDragWidth: 200,
      body: getCurrentScreen(context),
    );
  }
}
