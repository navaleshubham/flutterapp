import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
import 'package:my_society_digital_society/presentation/pages/dashboard.dart';
import 'package:my_society_digital_society/presentation/pages/signin.dart';

class Index extends StatelessWidget {
  const Index({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool auth = context.watch<MainCubit>().state.isauthenticated == null
        ? false
        : context.watch<MainCubit>().state.isauthenticated!;
    return SafeArea(
      child: auth ? Dashboard() : Signin(),
    );
  }
}
