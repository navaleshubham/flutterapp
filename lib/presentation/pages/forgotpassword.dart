import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/constants/routes.dart';
import 'package:my_society_digital_society/logic/cubits/auth/auth_cubit.dart';
import 'package:my_society_digital_society/presentation/widgets/authheader.dart';
import 'package:my_society_digital_society/presentation/widgets/fullScreenButtons.dart';
import 'package:my_society_digital_society/presentation/widgets/getemail.dart';
import 'package:my_society_digital_society/presentation/widgets/materialbuttons.dart';
import 'package:my_society_digital_society/presentation/widgets/showsnakbar.dart';

class ForgotPassword extends StatelessWidget {
  const ForgotPassword({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var error = context.watch<AuthCubit>().state.error == null
        ? ''
        : context.watch<AuthCubit>().state.error;
    var loading = context.watch<AuthCubit>().state.loading == null
        ? false
        : context.watch<AuthCubit>().state.loading!;
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            loading
                ? LinearProgressIndicator(
                    minHeight: 5,
                  )
                : new Container(width: 0.0, height: 0.0),
            AuthHeader(
              head1: 'Forgotten Your Password?',
              head2: 'Don`t Worry, We Are Here...!',
            ),
            SizedBox(height: 40),
            GetEmailInput(
              label: 'Email ID',
              onchange: (String value) {
                context.read<AuthCubit>().emailchange(value);
              },
            ),
            error != ''
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: Text(
                      error!,
                      style: TextStyle(color: Colors.red, fontSize: 18),
                    ),
                  )
                : FullWidthButtons(
                    buttonname: 'Send Reset Password Link',
                    margin: 30,
                    borderradius: 50,
                    onPress: () {
                      String? email = context.read<AuthCubit>().state.email;
                      if (email == null || email == '') {
                        showsnakbar(context, 'Please provide email');
                      } else {
                        context.read<AuthCubit>().resetpassword(email);
                      }
                    },
                  ),
            MaterialButtons(
              textbeforebutton: 'Done with resetting password?',
              buttoncontent: 'Sign In',
              horizontalpadding: 30,
              verticalpadding: 10,
              decoration: true,
              onpress: () {
                Navigator.of(context).popAndPushNamed(signin);
              },
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text(
                "Continue with Social Media",
                style: TextStyle(color: Colors.grey[600], fontSize: 16),
              ),
            ),
            Flex(
              mainAxisAlignment: MainAxisAlignment.center,
              direction: Axis.horizontal,
              children: [
                MaterialButtons(
                  buttoncontent: 'google plus',
                  horizontalpadding: 10,
                  width: 150,
                  color: Colors.redAccent,
                  decoration: true,
                  onpress: () {
                    showsnakbar(context, 'Sign up ');
                  },
                ),
                MaterialButtons(
                  buttoncontent: 'facebook',
                  horizontalpadding: 10,
                  width: 150,
                  decoration: true,
                  onpress: () {
                    showsnakbar(context, 'Sign up ');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
