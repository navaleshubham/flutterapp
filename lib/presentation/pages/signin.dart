import 'package:flutter/material.dart';
import 'package:my_society_digital_society/constants/routes.dart';
import 'package:my_society_digital_society/logic/cubits/auth/auth_cubit.dart';
import 'package:my_society_digital_society/presentation/widgets/fullScreenButtons.dart';
import 'package:my_society_digital_society/presentation/widgets/authheader.dart';
import 'package:my_society_digital_society/presentation/widgets/getemail.dart';
import 'package:my_society_digital_society/presentation/widgets/getpassword.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/presentation/widgets/materialbuttons.dart';
import 'package:my_society_digital_society/presentation/widgets/showsnakbar.dart';

class Signin extends StatelessWidget {
  const Signin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var loading = context.watch<AuthCubit>().state.loading == null
        ? false
        : context.watch<AuthCubit>().state.loading!;
    var error = context.watch<AuthCubit>().state.error == null
        ? ''
        : context.watch<AuthCubit>().state.error;
    return SafeArea(
      child: Scaffold(
        body: ListView(
          children: [
            loading
                ? LinearProgressIndicator(
                    minHeight: 5,
                  )
                : new Container(width: 0.0, height: 0.0),
            SizedBox(
              height: 20,
            ),
            AuthHeader(
              head1: 'Sign in',
              head2: 'Welcome Back !!',
            ),
            SizedBox(height: 40),
            GetEmailInput(
              label: 'Email ID',
              onchange: (String value) {
                context.read<AuthCubit>().emailchange(value);
              },
            ),
            SizedBox(height: 10),
            GetPasswordInput(
              lable: 'Password',
              onchange: (String value) {
                context.read<AuthCubit>().passwordchange(value);
              },
            ),
            error != ''
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: Text(
                      error!,
                      style: TextStyle(color: Colors.red, fontSize: 18),
                    ),
                  )
                : new Container(width: 0.0, height: 0.0),
            FullWidthButtons(
              buttonname: 'Sign in',
              margin: 30,
              borderradius: 50,
              onPress: () {
                String? email = context.read<AuthCubit>().state.email;
                String? password = context.read<AuthCubit>().state.password;
                if (email == null ||
                    email == '' ||
                    password == null ||
                    password == '') {
                  showsnakbar(
                      context, 'Please provide email and password both');
                } else {
                  context.read<AuthCubit>().login(context, email, password);
                }
              },
            ),
            MaterialButtons(
              buttoncontent: 'forgot password?',
              onpress: () {
                Navigator.of(context).pushNamed(forgotpassword);
              },
            ),
            MaterialButtons(
              textbeforebutton: 'Not have Account With us?',
              buttoncontent: 'Sign Up',
              horizontalpadding: 20,
              decoration: true,
              onpress: () {
                Navigator.of(context).pushNamed(signup);
              },
            ),
            SizedBox(height: 10),
            Center(
              child: Text(
                "Continue with Social Media",
                style: TextStyle(color: Colors.grey[600], fontSize: 16),
              ),
            ),
            Flex(
              mainAxisAlignment: MainAxisAlignment.center,
              direction: Axis.horizontal,
              children: [
                MaterialButtons(
                  buttoncontent: 'google plus',
                  horizontalpadding: 10,
                  width: 150,
                  color: Colors.redAccent,
                  decoration: true,
                  onpress: () {
                    showsnakbar(context, 'Sign up ');
                  },
                ),
                MaterialButtons(
                  buttoncontent: 'facebook',
                  horizontalpadding: 10,
                  width: 150,
                  decoration: true,
                  onpress: () {
                    showsnakbar(context, 'Sign up ');
                  },
                ),
                SizedBox(
                  height: 20,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
