import 'package:flutter/material.dart';
import 'package:my_society_digital_society/presentation/pages/dashboard.dart';
import 'package:my_society_digital_society/presentation/pages/forgotpassword.dart';
import 'package:my_society_digital_society/presentation/pages/index.dart';
import 'package:my_society_digital_society/presentation/pages/signin.dart';
import 'package:my_society_digital_society/presentation/pages/signup.dart';
import 'package:my_society_digital_society/constants/routes.dart';

class AppRouter {
  Route onGenratedRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case index:
        return MaterialPageRoute(builder: (_) => Index());
      case signup:
        return MaterialPageRoute(builder: (_) => Signup());
      case signin:
        return MaterialPageRoute(builder: (_) => Signin());
      case forgotpassword:
        return MaterialPageRoute(builder: (_) => ForgotPassword());
      case dashboard:
        return MaterialPageRoute(builder: (_) => Dashboard());
      default:
        return MaterialPageRoute(builder: (_) => Signin());
    }
  }
}
