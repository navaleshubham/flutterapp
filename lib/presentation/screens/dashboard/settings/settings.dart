import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:my_society_digital_society/presentation/widgets/materialbuttons.dart';
import 'package:my_society_digital_society/presentation/widgets/showsnakbar.dart';

class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        child: ListView(
          children: [
            SizedBox(
              height: 100,
            ),
            MaterialButtons(
              buttoncontent: 'terms and condition',
              onpress: () {
                showsnakbar(context, 'terms and condition');
              },
            ),
            MaterialButtons(
              buttoncontent: 'About Application',
              onpress: () {
                showsnakbar(context, 'About app');
              },
            ),
            MaterialButtons(
              buttoncontent: 'About Developers',
              onpress: () {
                showsnakbar(context, 'Developers');
              },
            ),
          ],
        ),
      ),
    );
  }
}
