import 'package:flutter/material.dart';
import 'package:my_society_digital_society/logic/cubits/profileauth/profileauth_cubit.dart';
import 'package:my_society_digital_society/presentation/widgets/fullScreenButtons.dart';
import 'package:my_society_digital_society/presentation/widgets/getpassword.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/presentation/widgets/showsnakbar.dart';

class AuthenticationInformation extends StatelessWidget {
  const AuthenticationInformation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool loading = (context.watch<ProfileauthCubit>().state.loading == null
        ? false
        : context.watch<ProfileauthCubit>().state.loading)!;
    var error = context.watch<ProfileauthCubit>().state.error == null
        ? ''
        : context.watch<ProfileauthCubit>().state.error;
    return Container(
      child: ListView(
        children: [
          loading
              ? LinearProgressIndicator(
                  minHeight: 5,
                  backgroundColor: Colors.red,
                )
              : new Container(width: 0.0, height: 0.0),
          SizedBox(
            height: 20,
          ),
          error != ''
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                  child: Text(
                    error!,
                    style: TextStyle(color: Colors.red, fontSize: 18),
                  ),
                )
              : new Container(width: 0.0, height: 0.0),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                GetPasswordInput(
                    onchange: (String value) => {
                          context
                              .read<ProfileauthCubit>()
                              .oldpasswordchange(value)
                        },
                    lable: 'Current Password'),
                GetPasswordInput(
                    onchange: (String value) => {
                          context
                              .read<ProfileauthCubit>()
                              .newpasswordchange(value)
                        },
                    lable: 'New Password'),
                GetPasswordInput(
                  onchange: (String value) => {
                    context
                        .read<ProfileauthCubit>()
                        .confirmpasswordchange(value)
                  },
                  lable: 'Confirm Password',
                ),
                SizedBox(
                  height: 20,
                ),
                FullWidthButtons(
                  buttonname: 'Change Password',
                  onPress: () => {
                    if (error == '')
                      {context.read<ProfileauthCubit>().changePassword()}
                    else
                      {showsnakbar(context, 'Please Resolve the errors first')}
                  },
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
