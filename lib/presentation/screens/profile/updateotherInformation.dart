import 'package:flutter/material.dart';
import 'package:my_society_digital_society/presentation/widgets/basicInput.dart';
import 'package:my_society_digital_society/logic/cubits/profileauth/profileauth_cubit.dart';
import 'package:my_society_digital_society/presentation/widgets/fullScreenButtons.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/presentation/widgets/showsnakbar.dart';

class OtherInformation extends StatelessWidget {
  const OtherInformation({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    bool loading = (context.watch<ProfileauthCubit>().state.loading == null
        ? false
        : context.watch<ProfileauthCubit>().state.loading)!;
    var error = context.watch<ProfileauthCubit>().state.error == null
        ? ''
        : context.watch<ProfileauthCubit>().state.error;
    return SafeArea(
      child: Container(
        child: ListView(
          children: [
            loading
                ? LinearProgressIndicator(
                    minHeight: 5,
                    backgroundColor: Colors.red,
                  )
                : new Container(width: 0.0, height: 0.0),
            SizedBox(
              height: 20,
            ),
            error != ''
                ? Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 10),
                    child: Text(
                      error!,
                      style: TextStyle(color: Colors.red, fontSize: 18),
                    ),
                  )
                : new Container(width: 0.0, height: 0.0),
            SizedBox(
              height: 20,
            ),
            InputField(
              lable: 'Building Number',
            ),
            InputField(
              lable: 'Building Wing',
            ),
            InputField(
              lable: 'Flat Number',
            ),
            InputField(
              lable: 'Owner Name',
            ),
            SizedBox(
              height: 20,
            ),
            FullWidthButtons(
              buttonname: 'Update Adress',
              height: 35,
              margin: 15,
              borderradius: 10,
              onPress: () => {
                if (error == '')
                  {context.read<ProfileauthCubit>().changePassword()}
                else
                  {showsnakbar(context, 'Please Resolve the errors first')}
              },
            )
          ],
        ),
      ),
    );
  }
}
