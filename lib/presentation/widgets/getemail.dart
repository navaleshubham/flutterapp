import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/logic/cubits/auth/auth_cubit.dart';

class GetEmailInput extends StatelessWidget {
  final String label;
  final dynamic onchange;
  const GetEmailInput({Key? key, required this.label, @required this.onchange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(),
        child: TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            decoration: InputDecoration(
              labelText: label,
              icon: Icon(Icons.email),
            ),
            initialValue: context.watch<AuthCubit>().state.email,
            validator: (String? value) {
              if (value!.isEmpty) {
                return 'E-mail is required';
              }
              if (!EmailValidator.validate(value)) {
                return 'Please enter a valid E-mail ID';
              }
              return null;
            },
            onChanged: onchange),
      ),
    );
  }
}
