import 'package:flutter/material.dart';

class MaterialButtons extends StatelessWidget {
  final String buttoncontent;
  final dynamic onpress;
  final String? textbeforebutton;
  final double? height;
  final double? width;
  final Color? color;
  final bool decoration;
  final bool row;
  final double horizontalpadding;
  final double verticalpadding;
  final bool column;
  final Icon? icon;
  const MaterialButtons(
      {Key? key,
      required this.buttoncontent,
      required this.onpress,
      this.textbeforebutton,
      this.decoration = false,
      this.height,
      this.row = true,
      this.column = false,
      this.horizontalpadding = 0,
      this.verticalpadding = 0,
      this.icon,
      this.width,
      this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: textbeforebutton != null
          ? Center(
              child: column
                  ? Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: horizontalpadding,
                          vertical: verticalpadding),
                      child: Column(
                        children: [
                          beforebuttontext(),
                          mbutton(),
                        ],
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: horizontalpadding,
                          vertical: verticalpadding),
                      child: Row(
                        children: [
                          Expanded(
                            child: beforebuttontext(),
                          ),
                          Expanded(child: mbutton()),
                        ],
                      ),
                    ),
            )
          : Padding(
              padding: EdgeInsets.symmetric(
                  horizontal: horizontalpadding, vertical: verticalpadding),
              child: Center(child: mbutton()),
            ),
    );
  }

  Text beforebuttontext() {
    return Text(
      textbeforebutton!,
      style: TextStyle(fontSize: 16),
    );
  }

  Container mbutton() {
    return Container(
      height: height,
      width: width,
      margin: decoration ? EdgeInsets.only(top: 10) : null,
      decoration: decoration
          ? BoxDecoration(
              color: color is Color ? color : Colors.blue[600],
              borderRadius: BorderRadius.circular(50),
            )
          : null,
      child: MaterialButton(
        child: decoration
            ? Text(
                buttoncontent,
                style: TextStyle(color: Colors.white, fontSize: 16),
              )
            : Text(
                buttoncontent,
                style: TextStyle(color: Colors.blueGrey, fontSize: 16),
              ),
        onPressed: onpress,
      ),
    );
  }
}
