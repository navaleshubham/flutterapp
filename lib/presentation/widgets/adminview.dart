import 'package:flutter/material.dart';
import 'package:my_society_digital_society/constants/admin/adminoptions.dart';
import 'package:my_society_digital_society/constants/appbartitles.dart';
import 'package:my_society_digital_society/constants/screenNames.dart';
import 'package:my_society_digital_society/constants/usertypes.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AdminView extends StatelessWidget {
  final String usertype;
  const AdminView({
    Key? key,
    required this.usertype,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Restricted Area',
          style: TextStyle(
            color: Colors.red,
            fontWeight: FontWeight.bold,
            fontSize: 20,
          ),
        ),
        SizedBox(
          height: 10,
        ),
        ExpansionTile(
          leading: Icon(
            Icons.admin_panel_settings_outlined,
          ),
          title: Text(
            usertype.toUpperCase(),
            style: TextStyle(
              fontSize: 20,
            ),
          ),
          children: [
            ListTile(
              leading: Icon(Icons.verified_outlined),
              title: Text(manageNotice.toUpperCase()),
              onTap: () {
                context.read<MainCubit>().screenChange(notice, noticestitle);
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.content_paste_sharp),
              title: Text(manageComplaints.toUpperCase()),
              onTap: () {
                context
                    .read<MainCubit>()
                    .screenChange(complaints, complainttitle);
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.notifications),
              title: Text(manageNotifications.toUpperCase()),
              onTap: () {
                context
                    .read<MainCubit>()
                    .screenChange(notification, notificationstitle);
                Navigator.pop(context);
              },
            ),
            usertype == admin
                ? ListTile(
                    leading: Icon(Icons.account_balance_rounded),
                    title: Text(maintainance.toUpperCase()),
                    onTap: () {
                      context
                          .read<MainCubit>()
                          .screenChange(notification, notificationstitle);
                      Navigator.pop(context);
                    },
                  )
                : new Container(width: 0.0, height: 0.0),
          ],
        ),
      ],
    );
  }
}
