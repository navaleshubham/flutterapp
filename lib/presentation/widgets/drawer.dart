import 'package:flutter/material.dart';
import 'package:my_society_digital_society/constants/appbartitles.dart';
import 'package:my_society_digital_society/constants/screenNames.dart';
import 'package:my_society_digital_society/constants/usertypes.dart';
import 'package:my_society_digital_society/logic/cubits/auth/auth_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
import 'package:my_society_digital_society/presentation/widgets/adminview.dart';

class DashboardDrawer extends StatelessWidget {
  const DashboardDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(
              context.watch<MainCubit>().state.username!,
              style: TextStyle(color: Colors.yellow[200]),
            ),
            accountEmail: Text(
              context.watch<MainCubit>().state.username!.split('.')[0],
              style: TextStyle(color: Colors.yellow[200]),
            ),
            arrowColor: Colors.transparent,
            onDetailsPressed: () {
              print('hi');
            },
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                  ? Colors.blue
                  : Colors.white,
              child: Text(
                context.watch<MainCubit>().state.username![0],
                style: TextStyle(fontSize: 50.0),
              ),
            ),
          ),
          ExpansionTile(
            leading: Icon(
              Icons.supervised_user_circle_outlined,
            ),
            title: Text(
              profile.toUpperCase(),
              style: TextStyle(
                fontSize: 20,
              ),
            ),
            children: [
              ListTile(
                leading: Icon(Icons.verified_user_outlined),
                title: Text(profileauthinfo.toUpperCase()),
                onTap: () {
                  context
                      .read<MainCubit>()
                      .screenChange(profileauthinfo, profileAuthInfotitle);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.person_add_alt_1_outlined),
                title: Text(profilePersnoal.toUpperCase()),
                onTap: () {
                  context
                      .read<MainCubit>()
                      .screenChange(profilePersnoal, profilepersonaltitle);
                  Navigator.pop(context);
                },
              ),
              ListTile(
                leading: Icon(Icons.info_outline_rounded),
                title: Text(profileOther.toUpperCase()),
                onTap: () {
                  context
                      .read<MainCubit>()
                      .screenChange(profileOther, profileothertitle);
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text(home.toUpperCase()),
            onTap: () {
              context.read<MainCubit>().screenChange(home, homepagetitle);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.settings),
            title: Text(setting.toUpperCase()),
            onTap: () {
              context.read<MainCubit>().screenChange(setting, settingtitle);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.message),
            title: Text(message.toUpperCase()),
            onTap: () {
              context.read<MainCubit>().screenChange(message, messagetitle);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.content_paste_sharp),
            title: Text(complaints.toUpperCase()),
            onTap: () {
              context
                  .read<MainCubit>()
                  .screenChange(complaints, complainttitle);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.notifications),
            title: Text(notification.toUpperCase()),
            onTap: () {
              context
                  .read<MainCubit>()
                  .screenChange(notification, notificationstitle);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.verified_outlined),
            title: Text(notice.toUpperCase()),
            onTap: () {
              context.read<MainCubit>().screenChange(notice, noticestitle);
              Navigator.pop(context);
            },
          ),
          ListTile(
            leading: Icon(Icons.history_sharp),
            title: Text(maintainanceHistory.toUpperCase()),
            onTap: () {
              context
                  .read<MainCubit>()
                  .screenChange(maintainanceHistory, maintainanceHistorytitle);
              Navigator.pop(context);
            },
          ),
          context.watch<MainCubit>().state.usertype.toString() == admin
              ? AdminView(
                  usertype:
                      context.watch<MainCubit>().state.usertype.toString(),
                )
              : new Container(width: 0.0, height: 0.0),
          SizedBox(
            height: 20,
          ),
          ListTile(
            leading: Icon(
              Icons.logout,
              color: Colors.white,
            ),
            title: Text(
              logout.toUpperCase(),
              style: TextStyle(color: Colors.white),
            ),
            tileColor: Colors.red[400],
            onTap: () {
              context.read<AuthCubit>().signout(context);
            },
          ),
        ],
      ),
    );
  }
}
