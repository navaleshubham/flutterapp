import 'package:flutter/material.dart';
import 'package:my_society_digital_society/constants/getBottomBarChange.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';

class DashboardBottomNav extends StatelessWidget {
  const DashboardBottomNav({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CurvedNavigationBar(
      items: [
        Icon(
          Icons.home,
          color: Colors.white,
          size: 25,
        ),
        Icon(
          Icons.content_paste_sharp,
          color: Colors.white,
          size: 25,
        ),
        Icon(
          Icons.fact_check_outlined,
          color: Colors.white,
          size: 25,
        ),
        Icon(
          Icons.message_outlined,
          color: Colors.white,
          size: 25,
        ),
      ],
      height: 60,
      color: Colors.blue,
      backgroundColor: Colors.transparent,
      buttonBackgroundColor: Colors.blue,
      animationDuration: Duration(
        milliseconds: 200,
      ),
      animationCurve: Curves.bounceInOut,
      onTap: (index) {
        bottomBarNav(context, index);
      },
    );
  }
}
