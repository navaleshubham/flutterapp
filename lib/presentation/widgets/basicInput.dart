import 'package:flutter/material.dart';

class InputField extends StatelessWidget {
  final String lable;
  final double paddingt;
  final double paddingb;
  final double paddingr;
  final double paddingl;
  final IconData? icons;
  final dynamic validators;
  final dynamic initvalue;
  final dynamic onchange;
  final bool all;

  const InputField(
      {Key? key,
      required this.lable,
      this.paddingt = 0,
      this.icons,
      this.validators,
      this.initvalue,
      this.onchange,
      this.paddingb = 0,
      this.paddingr = 0,
      this.paddingl = 0,
      this.all = true})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: all
            ? EdgeInsets.all(10)
            : EdgeInsets.only(
                top: paddingt,
                bottom: paddingb,
                right: paddingr,
                left: paddingl),
        decoration: BoxDecoration(),
        child: TextFormField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          decoration: InputDecoration(
            labelText: lable,
            icon: Icon(icons),
          ),
          initialValue: initvalue,
          validator: validators,
          onChanged: onchange,
        ),
      ),
    );
  }
}
