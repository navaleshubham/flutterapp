import 'package:flutter/material.dart';

class FullWidthButtons extends StatelessWidget {
  final String buttonname;
  final dynamic onPress;
  final double? height;
  final double? width;
  final double margin;
  final Color? color;
  final double borderradius;
  const FullWidthButtons(
      {Key? key,
      required this.buttonname,
      required this.onPress,
      this.height,
      this.width,
      this.margin = 0,
      this.color,
      this.borderradius = 0})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      // child: Expanded(
      child: Container(
        height: height,
        width: width,
        margin: EdgeInsets.all(margin),
        decoration: BoxDecoration(
          color: color is Color ? color : Colors.blue[600],
          borderRadius: BorderRadius.circular(borderradius),
        ),
        child: MaterialButton(
          child: Text(
            buttonname,
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),
          onPressed: onPress,
        ),
      ),
    );
    // );
  }
}
