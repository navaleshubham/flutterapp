import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/logic/cubits/auth/auth_cubit.dart';

class GetPasswordInput extends StatelessWidget {
  final String lable;
  final dynamic onchange;
  const GetPasswordInput({Key? key, required this.lable, this.onchange})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var visibility = context.watch<AuthCubit>().state.passwordVisiblity;
    bool pass = visibility == null
        ? true
        : context.watch<AuthCubit>().state.passwordVisiblity!;
    return Container(
      padding: EdgeInsets.all(10),
      child: TextFormField(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          obscureText: pass,
          decoration: InputDecoration(
            labelText: lable,
            icon: Icon(Icons.lock),
            suffixIcon: InkWell(
              child: pass
                  ? Icon(Icons.visibility_outlined)
                  : Icon(Icons.visibility_off_outlined),
              onTap: () => {context.read<AuthCubit>().passwordvisibility()},
            ),
          ),
          keyboardType: TextInputType.visiblePassword,
          validator: (String? value) {
            if (value!.isEmpty) {
              return 'Password is required';
            }
            if (value.length < 8) {
              return 'Password should at least have 8 characters';
            }
            return null;
          },
          onChanged: onchange),
    );
  }
}
