import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:my_society_digital_society/logic/cubits/auth/auth_cubit.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
import 'package:my_society_digital_society/logic/cubits/profileauth/profileauth_cubit.dart';
import 'package:my_society_digital_society/presentation/router/main_screen_router.dart';
import 'package:path_provider/path_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  HydratedBloc.storage = await HydratedStorage.build(
    storageDirectory: kIsWeb
        ? HydratedStorage.webStorageDirectory
        : await getTemporaryDirectory(),
  );
  await Firebase.initializeApp();
  runApp(MyApp(
    appRouter: AppRouter(),
  ));
}

class MyApp extends StatelessWidget {
  final AppRouter? appRouter;

  const MyApp({Key? key, this.appRouter}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthCubit>(
          create: (context) => AuthCubit(),
        ),
        BlocProvider<MainCubit>(
          create: (context) => MainCubit(),
        ),
        BlocProvider<ProfileauthCubit>(
          create: (context) => ProfileauthCubit(),
        )
      ],
      child: MaterialApp(
        // theme:context.watch<MainCubit>().state.darktheme,
        darkTheme: ThemeData.dark(),
        theme: ThemeData(primarySwatch: Colors.blue),
        debugShowCheckedModeBanner: false,
        onGenerateRoute: appRouter!.onGenratedRoute,
      ),
    );
  }
}
