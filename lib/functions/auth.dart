import 'package:firebase_auth/firebase_auth.dart';

Future<dynamic> signin(String email, String password) async {
  try {
    dynamic data = await FirebaseAuth.instance
        .signInWithEmailAndPassword(email: email, password: password);
    return data;
  } on FirebaseAuthException catch (e) {
    return e.message;
  } catch (e) {
    return e.toString();
  }
}

Future<dynamic> signup(String email, String password) async {
  try {
    dynamic data = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email, password: password);
    return data;
  } on FirebaseAuthException catch (e) {
    return e.message;
  } catch (e) {
    return e;
  }
}

Future getcurrentuser() async {
  try {
    dynamic cuser = FirebaseAuth.instance.currentUser;

    return cuser;
  } on FirebaseAuthException catch (e) {
    return e.message;
  }
}

Future<dynamic> logout() async {
  try {
    await FirebaseAuth.instance.signOut();
    return true;
  } on FirebaseAuthException catch (e) {
    return e.message;
  }
}

Future<dynamic> resetPassword(String email) async {
  try {
    return await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
  } on FirebaseAuthException catch (e) {
    return e.message;
  } catch (e) {
    print(e.toString());
    return e.toString();
  }
}
