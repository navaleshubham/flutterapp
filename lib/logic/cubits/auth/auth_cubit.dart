import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:my_society_digital_society/functions/auth.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit()
      : super(
          AuthState(passwordVisiblity: true),
        );
  void emailchange(String value) {
    emit(state.copyWith(email: value, error: ''));
  }

  void passwordchange(String value) {
    emit(state.copyWith(password: value, error: ''));
  }

  void passwordvisibility() {
    emit(state.copyWith(
      passwordVisiblity: !state.passwordVisiblity!,
    ));
  }

  void login(BuildContext context, String email, String password) async {
    emit(state.copyWith(
        loading: true, email: email, password: password, error: ''));
    dynamic data = await signin(email, password);
    if (data is String) {
      print(data);
      emit(state.copyWith(
        loading: false,
        error: data.toString(),
      ));
    } else {
      context.read<MainCubit>().authSuccess(email);
      emit(state.copyWith(
        email: email,
        password: '',
        loading: false,
        error: '',
      ));
    }
  }

  void newuser(BuildContext context, String email, String password) async {
    emit(state.copyWith(loading: true, email: email));
    dynamic data = await signup(email, password);
    print(data);

    if (data is String) {
      print(data);
      emit(state.copyWith(
        email: email,
        password: null,
        loading: false,
        error: data.toString(),
      ));
    } else {
      context.read<MainCubit>().authSuccess(email);
      emit(state.copyWith(
          email: email,
          password: null,
          loading: false,
          error: '',
          response: data.toString()));
    }
  }

  void resetpassword(String email) async {
    emit(state.copyWith(loading: true, email: email));
    dynamic data = await resetPassword(email);
    if (data is String) {
      emit(state.copyWith(error: data, loading: false));
    } else {
      emit(state.copyWith(
          error: 'Please check your email for password reset link!!',
          loading: false));
    }
  }

  void signout(BuildContext context) async {
    emit(state.copyWith(loading: true));
    dynamic data = await logout();
    if (data is String) {
      emit(state.copyWith(error: data, loading: false));
    } else if (data) {
      context.read<MainCubit>().unauthstate();
      emit(state.copyWith(email: '', loading: false));
    } else {
      emit(state.copyWith(email: '', loading: false));
    }
  }
}
