part of 'auth_cubit.dart';

class AuthState extends Equatable {
  final String? email;
  final String? password;
  final bool? passwordVisiblity;
  final bool? loading;
  final String? error;
  final String? response;
  const AuthState(
      {this.email,
      this.password,
      this.passwordVisiblity,
      this.loading,
      this.error,
      this.response});
  AuthState copyWith(
      {String? email,
      String? password,
      bool? passwordVisiblity,
      bool? loading,
      String? error,
      String? response}) {
    return AuthState(
        email: email ?? this.email,
        password: password ?? this.password,
        passwordVisiblity: passwordVisiblity ?? this.passwordVisiblity,
        loading: loading ?? this.loading,
        error: error ?? this.error,
        response: response ?? this.response);
  }

  @override
  List<Object?> get props {
    return [
      email,
      password,
      passwordVisiblity,
      loading,
      error,
      response,
    ];
  }

  @override
  bool get stringify => true;
}

class AuthInitial extends AuthState {}
