import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'profileother_state.dart';

class ProfileotherCubit extends Cubit<ProfileotherState> {
  ProfileotherCubit() : super(ProfileotherInitial());
}
