part of 'profileother_cubit.dart';

abstract class ProfileotherState extends Equatable {
  const ProfileotherState();

  @override
  List<Object> get props => [];
}

class ProfileotherInitial extends ProfileotherState {}
