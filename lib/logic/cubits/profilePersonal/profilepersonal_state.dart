part of 'profilepersonal_cubit.dart';

abstract class ProfilepersonalState extends Equatable {
  const ProfilepersonalState();

  @override
  List<Object> get props => [];
}

class ProfilepersonalInitial extends ProfilepersonalState {}
