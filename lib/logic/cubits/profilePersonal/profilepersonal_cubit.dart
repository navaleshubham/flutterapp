import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'profilepersonal_state.dart';

class ProfilepersonalCubit extends Cubit<ProfilepersonalState> {
  ProfilepersonalCubit() : super(ProfilepersonalInitial());
}
