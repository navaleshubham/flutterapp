import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'profileauth_state.dart';

class ProfileauthCubit extends Cubit<ProfileauthState> {
  ProfileauthCubit() : super(ProfileauthInitial());
  void newpasswordchange(String value) {
    emit(state.copywith(newPassword: value, error: ''));
  }

  void oldpasswordchange(String value) {
    emit(state.copywith(oldPassword: value, error: ''));
  }

  void confirmpasswordchange(String value) {
    emit(state.copywith(
      confirmPassword: value,
      error: '',
    ));
  }

  void phonenumber(String value) {
    try {
      // print(value);
      int number = int.parse(value);
      print(number);
      if (value.length != 10) {
        emit(state.copywith(error: 'Number must be 10 digit', loading: false));
      } else {
        emit(state.copywith(error: ''));
      }
    } catch (e) {
      emit(state.copywith(
          error: 'Number must type of Int only', loading: false));
    }
  }

  void changePassword() {
    emit(state.copywith(loading: true));
    if (state.confirmPassword != state.newPassword) {
      emit(state.copywith(
        error: 'new password and confirm password not matching',
        loading: false,
      ));
    } else {
      emit(state.copywith(
        loading: false,
      ));
    }
  }
}
