part of 'profileauth_cubit.dart';

class ProfileauthState extends Equatable {
  final String? oldPassword;
  final String? newPassword;
  final String? confirmPassword;
  final String? phone;
  final String? alternativemail;
  final String? otp;
  final bool? passwordVisiblity;
  final bool? loading;
  final String? error;
  const ProfileauthState(
      {this.oldPassword,
      this.newPassword,
      this.confirmPassword,
      this.phone,
      this.alternativemail,
      this.otp,
      this.passwordVisiblity,
      this.loading,
      this.error});

  ProfileauthState copywith({
    String? oldPassword,
    String? newPassword,
    String? confirmPassword,
    String? phone,
    String? alternativemail,
    String? otp,
    bool? passwordVisiblity,
    bool? loading,
    String? error,
  }) {
    return ProfileauthState(
      oldPassword: oldPassword ?? this.oldPassword,
      newPassword: newPassword ?? this.newPassword,
      confirmPassword: confirmPassword ?? this.confirmPassword,
      phone: phone ?? this.phone,
      alternativemail: alternativemail ?? this.alternativemail,
      otp: otp ?? this.otp,
      passwordVisiblity: passwordVisiblity ?? this.passwordVisiblity,
      loading: loading ?? this.loading,
      error: error ?? this.error,
    );
  }

  @override
  List<Object?> get props => [
        oldPassword,
        newPassword,
        confirmPassword,
        phone,
        alternativemail,
        otp,
        passwordVisiblity,
        loading,
        error,
      ];

  @override
  bool get stringify => true;

  Map<String, dynamic> toMap() {
    return {
      'oldPassword': oldPassword,
      'newPassword': newPassword,
      'confirmPassword': confirmPassword,
      'phone': phone,
      'alternativemail': alternativemail,
      'otp': otp,
      'passwordVisiblity': passwordVisiblity,
      'loading': loading,
      'error': error,
    };
  }

  factory ProfileauthState.fromMap(Map<String, dynamic> map) {
    return ProfileauthState(
      oldPassword: map['oldPassword'],
      newPassword: map['newPassword'],
      confirmPassword: map['confirmPassword'],
      phone: map['phone'],
      alternativemail: map['alternativemail'],
      otp: map['otp'],
      passwordVisiblity: map['passwordVisiblity'],
      loading: map['loading'],
      error: map['error'],
    );
  }

  String toJson() => json.encode(toMap());

  factory ProfileauthState.fromJson(String source) =>
      ProfileauthState.fromMap(json.decode(source));
}

class ProfileauthInitial extends ProfileauthState {}
