import 'package:bloc/bloc.dart';
import 'dart:convert';
import 'package:equatable/equatable.dart';
import 'package:hydrated_bloc/hydrated_bloc.dart';
import 'package:my_society_digital_society/constants/appbartitles.dart';
import 'package:my_society_digital_society/constants/screenNames.dart';
import 'package:my_society_digital_society/functions/auth.dart';
part 'main_state.dart';

class MainCubit extends Cubit<MainState> with HydratedMixin {
  MainCubit() : super(MainInitial());

  void authSuccess(String email) {
    print(email);
    emit(state.copyWith(username: email, isauthenticated: true));
  }

  void unauthstate() {
    emit(state.copyWith(isauthenticated: false));
  }

  void checkcurrentuser() async {
    dynamic data = await getcurrentuser();
    if (data is String) {
      print(data);
      emit(state.copyWith(
        username: null,
        userid: null,
        usertype: null,
        isauthenticated: false,
      ));
    } else {
      print(data);
      emit(state.copyWith(
        isauthenticated: true,
      ));
    }
  }

  void themechange() {
    emit(state.copyWith(darktheme: !state.darktheme));
  }

  void screenChange(String screen, String appbartitle) {
    emit(state.copyWith(currentscreen: screen, appBarTitle: appbartitle));
  }

  void bottomnavchange(String screen, String appbartitle) {
    emit(state.copyWith(currentscreen: screen, appBarTitle: appbartitle));
  }

  @override
  MainState fromJson(Map<String, dynamic> json) {
    return MainState.fromMap(json);
  }

  @override
  Map<String, dynamic> toJson(MainState state) {
    return state.toMap();
  }
}
