part of 'main_cubit.dart';

class MainState extends Equatable {
  final String? username;
  final String? userid;
  final String? usertype;
  final bool? isauthenticated;
  final bool darktheme;
  final String currentscreen;
  final String appBarTitle;

  const MainState({
    this.username,
    this.userid,
    this.usertype,
    this.darktheme = false,
    this.currentscreen = home,
    this.appBarTitle = homepagetitle,
    this.isauthenticated,
  });

  MainState copyWith({
    String? username,
    String? userid,
    String? usertype,
    bool? isauthenticated,
    bool? darktheme,
    String? appBarTitle,
    String? currentscreen,
  }) {
    return MainState(
      username: username ?? this.username,
      userid: userid ?? this.userid,
      usertype: usertype ?? this.usertype,
      appBarTitle: appBarTitle ?? this.appBarTitle,
      isauthenticated: isauthenticated ?? this.isauthenticated,
      darktheme: darktheme ?? this.darktheme,
      currentscreen: currentscreen ?? this.currentscreen,
    );
  }

  @override
  bool get stringify => true;

  @override
  List<Object?> get props {
    return [
      username,
      userid,
      usertype,
      isauthenticated,
      currentscreen,
      darktheme,
      appBarTitle,
    ];
  }

  Map<String, dynamic> toMap() {
    return {
      'username': username,
      'userid': userid,
      'usertype': usertype,
      'darktheme': darktheme,
      'isauthenticated': isauthenticated,
    };
  }

  factory MainState.fromMap(Map<String, dynamic> map) {
    return MainState(
      username: map['username'],
      userid: map['userid'],
      usertype: map['usertype'],
      isauthenticated: map['isauthenticated'],
    );
  }
  String toJson() => json.encode(toMap());
  factory MainState.fromJson(String source) =>
      MainState.fromMap(json.decode(source));
}

class MainInitial extends MainState {}
