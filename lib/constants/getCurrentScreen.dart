import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/constants/screenNames.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/complaints/complaints.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/home/homeScreen.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/maintainance/maintainancehistory.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/messages/messages.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/notices/notices.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/notifications/notifications.dart';
import 'package:my_society_digital_society/presentation/screens/dashboard/settings/settings.dart';
import 'package:my_society_digital_society/presentation/screens/profile/updateAuthInfo.dart';
import 'package:my_society_digital_society/presentation/screens/profile/updatepersonalInformation.dart';
import 'package:my_society_digital_society/presentation/screens/profile/updateotherInformation.dart';

dynamic getCurrentScreen(BuildContext context) {
  print(context.watch<MainCubit>().state.currentscreen.toString());
  switch (context.watch<MainCubit>().state.currentscreen.toString()) {
    case profilePersnoal:
      return PersonalInformation();
    case profileOther:
      return OtherInformation();
    case profileauthinfo:
      return AuthenticationInformation();
    case home:
      return HomeScreen();
    case setting:
      return Settings();
    case message:
      return Messages();
    case complaints:
      return Complaints();
    case notification:
      return NotificationsScreen();
    case notice:
      return Notices();
    case maintainanceHistory:
      return MaintainanceHistory();
    default:
      return HomeScreen();
  }
}
