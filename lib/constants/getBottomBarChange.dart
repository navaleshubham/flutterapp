import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_society_digital_society/constants/appbartitles.dart';
import 'package:my_society_digital_society/constants/screenNames.dart';
import 'package:my_society_digital_society/logic/cubits/main/main_cubit.dart';

void bottomBarNav(BuildContext context, int index) {
  switch (index) {
    case 0:
      context.read<MainCubit>().screenChange(home, homepagetitle);
      break;
    // case 1:
    //   context.read<MainCubit>().screenChange(notification, notificationstitle);
    //   break;
    case 1:
      context.read<MainCubit>().screenChange(complaints, complainttitle);
      break;
    case 2:
      context.read<MainCubit>().screenChange(notice, noticestitle);
      break;
    case 3:
      context.read<MainCubit>().screenChange(message, messagetitle);
      break;
  }
}
